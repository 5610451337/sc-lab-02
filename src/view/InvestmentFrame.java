package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.BankAccount;

public class InvestmentFrame extends JFrame {
	private static final double DEFAULT_RATE = 5;

	private JLabel rateLabel;
	private JTextField rateField;
	private JButton button;
	private JLabel resultLabel;
	private JPanel panel;

	public InvestmentFrame(){  
	    resultLabel = new JLabel("");
	    createTextField();
	    createButton();
	    createPanel();
	   // setVisible(true);
	   // setSize(FRAME_WIDTH, FRAME_HEIGHT);
	   }

	   private void createTextField()
	   {
	      rateLabel = new JLabel("Interest Rate: ");

	      final int FIELD_WIDTH = 10;
	      rateField = new JTextField(FIELD_WIDTH);
	      rateField.setText("" + DEFAULT_RATE);
	   }
	   
	   private void createButton(){
	      button = new JButton("Add Interest");
	   }
	      
	   private void createPanel()
	   {
	      panel = new JPanel();
	      panel.add(rateLabel);
	      panel.add(rateField);
	      panel.add(button);
	      panel.add(resultLabel);      
	      add(panel);
	   }
	   
	   public void setBalance(BankAccount bank){
		   resultLabel.setText("balance: " + bank.getBalance());
	   }
	   
	   public void setListenner(ActionListener list){
		   button.addActionListener(list);
	   }

	public String getrateField() {
		return rateField.getText();
	}
	   
}
