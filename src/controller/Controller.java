package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.BankAccount;
import view.InvestmentFrame;

public class Controller {
	private InvestmentFrame frame ;
	private BankAccount account;
	private ActionListener list;
	
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 100;
	private static final double INITIAL_BALANCE = 1000;   
	
	public Controller(){
		account = new BankAccount( INITIAL_BALANCE);
		createFrame();
		createListener();
		frame.setBalance(account);
	}
    class AddInterestListener implements ActionListener
    {
       public void actionPerformed(ActionEvent event)
       {
          double rate = Double.parseDouble(frame.getrateField());
          double interest = account.getBalance() * rate / 100;
          account.deposit(interest);
          frame.setBalance(account);
       }            
    }

	public static void main (String[] args){
		new Controller();
	}
	
	public void createFrame(){
		frame = new InvestmentFrame();
		frame.setVisible(true);
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}
	public void createListener(){
		list = new AddInterestListener();
		frame.setListenner(list);
	}
}


